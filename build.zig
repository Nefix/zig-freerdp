const std = @import("std");

pub fn build(b: *std.build.Builder) !void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{
        .default_target = .{
            .cpu_arch = .wasm32,
            .os_tag = .freestanding,
        },
    });

    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions();

    const freerdp = b.addStaticLibrary("zig-freerdp", "src/main.zig");
    freerdp.setTarget(target);
    freerdp.setBuildMode(mode);
    freerdp.linkLibC();
    freerdp.addIncludeDir("/home/nefix/.cache/emscripten/sysroot/include");
    freerdp.linkSystemLibrary("freerdp3");
    freerdp.linkSystemLibrary("winpr3");
    freerdp.linkSystemLibrary("crypto");
    // freerdp.install();

    const emscripten = b.addSystemCommand(&.{
        "emcc",
        "-o",
        b.pathJoin(&.{ b.install_path, "zig.html" }),
        "-s",
        "EXPORTED_FUNCTIONS=['_main']",
        "-O3",
        "-L/nix/store/8k5035z0145j6v45dvrivk2szl6m7f5q-emscripten-freerdp-3.0.0-8561b08/lib",
        "-lfreerdp3",
        "-lwinpr3",
        "-L/nix/store/x2sm3nggn30154z7yfbarh3j024hrkzb-emscripten-openssl-1.1.1q/lib",
        "-lcrypto",
    });
    emscripten.addFileSourceArg(freerdp.getOutputSource());
    // emscripten.step.dependOn(&freerdp.install_step.?.step);

    b.default_step.dependOn(&emscripten.step);

    // const exe = b.addExecutable("zig-freerdp", "src/main.zig");
    // exe.setTarget(target);
    // exe.setBuildMode(mode);
    // exe.linkLibC();
    // exe.linkSystemLibrary("freerdp3");
    // exe.linkSystemLibrary("winpr3");
    // exe.install();
    // exe.step.dependOn(&freerdp.step);

    // const run_cmd = exe.run();
    // run_cmd.step.dependOn(b.getInstallStep());
    // if (b.args) |args| {
    //     run_cmd.addArgs(args);
    // }

    //     const run_step = b.step("run", "Run the app");
    //     run_step.dependOn(&run_cmd.step);

    //     const exe_tests = b.addTest("src/main.zig");
    //     exe_tests.setTarget(target);
    //     exe_tests.setBuildMode(mode);

    //     const test_step = b.step("test", "Run unit tests");
    //     test_step.dependOn(&exe_tests.step);
}
