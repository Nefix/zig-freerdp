const std = @import("std");
const c = @cImport({
    @cInclude("freerdp/freerdp.h");
    @cInclude("emscripten.h");
});

export fn main() void {
    // const allocator = std.heap.page_allocator;

    const rdp = @ptrCast(*c.freerdp, c.freerdp_new());
    defer c.freerdp_free(rdp);

    // const the_type = std.fmt.allocPrint(allocator, "{}", .{@typeInfo(@TypeOf(rdp))}) catch |err| {
    //     c.emscripten_log(c.EM_LOG_INFO, "out");
    // };
    // defer allocator.free(the_type);

    c.emscripten_log(c.EM_LOG_INFO, @typeName(@TypeOf(rdp)));
    c.emscripten_log(c.EM_LOG_INFO, "HOLA");
    // std.log.info("hola :D", .{});
    // rdp.PreConnect = preConnect;
}

// test "simple test" {
//     var list = std.ArrayList(i32).init(std.testing.allocator);
//     defer list.deinit(); // try commenting this out and see if zig detects the memory leak!
//     try list.append(42);
//     try std.testing.expectEqual(@as(i32, 42), list.pop());
// }
