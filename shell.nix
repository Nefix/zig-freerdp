{ pkgs ? import <nixpkgs> {}, zig }:
  let
  emOpenssl = pkgs.buildEmscriptenPackage rec {
    pname = "openssl";
    version = "1.1.1q";

    src = pkgs.fetchgit {
      url = "https://github.com/openssl/openssl";
      rev = "OpenSSL_1_1_1q";
      sha256 = "sha256-h7LU+Pz6O2drpZZt0WKVgT1k6u6FxIu3SwDIyFi/Ztw=";
    };

    buildInputs = with pkgs; [
      perl
      pkg-config
    ];

    preConfigure = ''
      echo "#!/bin/sh
CROSS_COMPILE="" perl Configure gcc --prefix=$out" > configure
      chmod +x configure
    '';

    buildPhase = ''
      HOME=$TMPDIR
      emmake make build_libs
    '';

    installPhase = ''
      HOME=$TMPDIR
      emmake make install_dev
    '';

    checkPhase = "";
  };

  emZlib = pkgs.buildEmscriptenPackage rec {
    pname = "zlib";
    version = "1.2.12";

    src = pkgs.fetchgit {
      url = "https://github.com/madler/zlib";
      rev = "v1.2.12";
      sha256 = "sha256-bIm5+uHv12/x2uqEbZ4/VGzUJnDzW9C3GkyHo3EnC1A=";
    };

    buildInputs = with pkgs; [
      pkg-config
      cmake
    ];

    configurePhase = ''
      emcmake cmake . -DCMAKE_INSTALL_PREFIX=$out
    '';

    buildPhase = ''
      emmake make
    '';

    installPhase = ''
      emmake make install
    '';

    checkPhase = "";
  };

  emFreerdp = pkgs.buildEmscriptenPackage rec {
    pname = "freerdp";
    version = "3.0.0-8561b08";

    src = pkgs.fetchgit {
      url = "https://github.com/FreeRDP/FreeRDP";
      rev = "8561b08";
      sha256 = "sha256-pG+/vm2WB9ZRQi8m4P9yA5nL1eWj0/cJXWz6GY1r86A=";
    };

    nativeBuildInputs = with pkgs; [
      pkg-config
    ];
    buildInputs = with pkgs; [
      cmake
      ninja
      emOpenssl
      emZlib
    ];

    preConfigure = ''
      echo "#include <time.h>
      $(cat libfreerdp/core/streamdump.c)" > libfreerdp/core/streamdump.c
    '';

    configurePhase = ''
      runHook preConfigure

      emcmake cmake . \
        -DCMAKE_INSTALL_PREFIX=$out \
        -DZLIB_INCLUDE_DIR=${emZlib}/include -DZLIB_LIBRARY=${emZlib}/lib/libz.a \
        -DOPENSSL_INCLUDE_DIR=${emOpenssl}/include -DOPENSSL_CRYPTO_LIBRARY=${emOpenssl}/lib/libcrypto.a -DOPENSSL_SSL_LIBRARY=${emOpenssl}/lib/libssl.a \
        -DWITH_LIBSYSTEMD=OFF -DWITH_MANPAGES=OFF -DWITH_X11=OFF -DWITH_WAYLAND=OFF -DCHANNEL_URBDRC=OFF -DBUILD_SHARED_LIBS=OFF -DWITH_SSE2=OFF -DWITH_SERVER_INTERFACE=OFF -DWITH_OSS=OFF -DWITH_EVENTFD_READ_WRITE=ON -DWITH_CUNIT=OFF -DBUILD_TESTING=OFF

      mkdir -p .emscriptencache
      export EM_CACHE=$(pwd)/.emscriptencache

      runHook postConfigure
    '';
  
    postConfigure = ''
      substituteInPlace winpr/libwinpr/winsock/winsock.c \
        --replace "#if !defined(__linux__) && !defined(__sun__) && !defined(__CYGWIN__)" \
          "#if !defined(__linux__) && !defined(__sun__) && !defined(__CYGWIN__) && !defined(EMSCRIPTEN)"
    '';

    buildPhase = ''
      cmake --build .
    '';

    installPhase = ''
      cmake --build . --target install
    '';

    checkPhase = "";
  };
  in

  pkgs.mkShell {
    nativeBuildInputs = with pkgs; [
      pkg-config
      zig
      cmake
      cmakeCurses
      wasmtime
      wabt
      emscripten
      emOpenssl
      emFreerdp
    ];

    shellHook = ''
      mkdir -p ~/.cache/emscripten
      export EM_CACHE=~/.cache/emscripten
    '';
}
